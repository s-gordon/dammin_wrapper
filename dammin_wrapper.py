#!/usr/bin/env python
# AUTHOR:   Shane Gordon
# FILE:     dammin_wrapper.py
# ROLE:     Python wrapper for CRYSOL
# CREATED:  2015-05-16 22:27:15
# MODIFIED: 2015-05-19 11:58:47

import argparse
import pexpect
import sys, os
import logging
import glob

logging.basicConfig(format='%(levelname)s:\t%(message)s', level=logging.DEBUG)

class MyParser(argparse.ArgumentParser):
    def error(self, message):
        sys.stderr.write('error: %s\n' % message)
        self.print_help()
        sys.exit(2)

def filecheck(file):
    try:
        f = open('%s' % file, 'r')
        f.close()
    except IOError:
        logging.error('There was an error opening the file: %s.' % file)
        logging.error('Have you spelt the file name correctly?')
        logging.error('Exiting')
        sys.exit(1)

def assure_path_exists(path):
    if not os.path.exists(path):
        os.makedirs(path)

parser=MyParser(description='Python wrapper for DAMMIN refinement of DAMAVER models',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)

parser.add_argument('-d', action="store", dest="dammin", 
        default="/usr/local/bin/crysol", help="DAMMIN executable directory")
parser.add_argument('-E', action="store", dest="gnom_datafile", 
        required = True, help="GNOM output file")
parser.add_argument('-p', action="store", dest="pdbfile", 
        required = True, help="User-defined pdb file for starting envelope")
parser.add_argument('-M', action="store", dest="mode", default="F",
		help="Mode: <[F]>ast, [S]low, [J]ag, [K]eep, [E]xpert")
parser.add_argument('-s', action="store", dest="portion_curve", default="1",
        type=float, help="Percentage of the scattering curve to fit, starting at the first point. The whole curve is used by default.")
parser.add_argument('-r', action="store", dest="reset_core", default="N",
        help="Reset core (unfix all atoms)")
parser.add_argument('-S', action="store", dest="shape", default="U",
		help="Expected particle shape: <P>rolate, <O>blate, or <U>nknown")
parser.add_argument('-P', action="store", dest="out_f", default="damminfit",
		help="Output prefix")

result = parser.parse_args()

filecheck(result.pdbfile)
filecheck(result.gnom_datafile)
filecheck(result.dammin)

pdb_prefix = os.path.splitext(os.path.basename(result.pdbfile))[0]
exp_prefix = os.path.splitext(os.path.basename(result.gnom_datafile))[0]
out_prefix = os.path.splitext(os.path.basename(result.out_f))[0]
out_dir = '{0}.{1}.DAMMIN'.format(pdb_prefix, exp_prefix)

assure_path_exists(out_dir)

child = pexpect.spawn('%s' % result.dammin)
child.logfile = sys.stdout
child.expect('Mode')
child.sendline('%s' % result.mode)
child.expect('Log file name')
child.sendline('%s' % out_prefix)
child.expect('Input data')
child.sendline('%s' % result.gnom_datafile)
child.expect('Enter project description')
child.sendline('%s' % '{0}.{1}.DAMMIN'.format(pdb_prefix, exp_prefix))
child.expect('theta')
child.sendline('1')
child.expect('Portion of the curve to be fitted')
child.sendline('%s' % result.portion_curve)
child.expect('Initial DAM')
child.sendline('%s' % result.pdbfile)
child.expect('Symmetry')
child.sendline('%s' % "P1")
child.expect('Reset core')
child.sendline('%s' % result.reset_core)
child.expect('Expected particle shape')
child.sendline('%s' % result.shape)
child.expect('===== DAMMIN finished at', timeout=None)
child.close()

for n in glob.glob('%s*' % out_prefix):
	if os.path.isfile(n):
		os.rename('%s' % n, '{0}/{1}'.format(out_dir, n))

exit
