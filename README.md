# README #

```sh
./dammin_wrapper -h
```

### What is this repository for? ###

* Quick summary
* Version 0.1b

### How do I install it ###

Easiest way to download is by using git:
```sh
git clone https://bitbucket.org/s-gordon/dammin_wrapper.git
```
Assuming you've got python 2.7+ installed along with pexpect (see dependencies below), you're good to go.

### How do I get set up? ###

* Dependencies
  - pexpect: `pip install pexpect`
* How to run tests
  - `./dammin_wrapper.py -h`

### Contribution guidelines ###

### Who do I talk to? ###

* Shane Gordon
